pragma optional_param default_ssl_check_cert 0;
C!
W!
# Backends

backend F_idge1_usw_www_idgesg_idg_tv {
    .connect_timeout = 2s;
    .dynamic = true;
    .port = "80";
    .host = "idge1.usw.www.idgesg.idg.tv";
    .first_byte_timeout = 20s;
    .max_connections = 200;
    .between_bytes_timeout = 20s;
    .share_key = "7L3Bo1bdUQV1DWKe82yOqc";
  
      
    .probe = {
        .request = "HEAD / HTTP/1.1"  "Host: www.idg.tv" "Connection: close";
        .window = 5;
        .threshold = 1;
        .timeout = 2s;
        .initial = 5;
        .dummy = true;
      }
}
backend F_apollo_sitemaps_s3 {
    .connect_timeout = 1s;
    .dynamic = true;
    .port = "80";
    .host = "apollo-sitemaps.s3-website-us-east-1.amazonaws.com";
    .first_byte_timeout = 15s;
    .max_connections = 200;
    .between_bytes_timeout = 10s;
    .share_key = "7L3Bo1bdUQV1DWKe82yOqc";
  
      
    .probe = {
        .request = "HEAD / HTTP/1.1"  "Host: www.idg.tv" "Connection: close";
        .window = 5;
        .threshold = 1;
        .timeout = 2s;
        .initial = 5;
        .dummy = true;
      }
}








## www.idg.tv
# custom acl
## included in top of main

# Who is allowed access ...
acl goodguys {
    "localhost";
    "38.99.32.0"/24; /* 501 Second */
    "206.80.3.0"/26; /* 501 Second */
    "206.80.4.64"/26; /* 501 Second */
    "70.42.185.0"/24; /* 365 Main */
    "23.235.32.0"/20;	/* FASTLY */
    "43.249.72.0"/22;	/* FASTLY */
    "103.244.50.0"/24;	/* FASTLY */
    "103.245.222.0"/23;	/* FASTLY */
    "103.245.224.0"/24;	/* FASTLY */
    "104.156.80.0"/20;	/* FASTLY */
    "157.52.64.0"/18;	/* FASTLY */
    "185.31.16.0"/22;	/* FASTLY */
    "199.27.72.0"/21;	/* FASTLY */
    "202.21.128.0"/24;	/* FASTLY */
    "203.57.145.0"/24;	/* FASTLY */
}


sub vcl_recv {



#--FASTLY RECV BEGIN
  if (req.restarts == 0) {
    if (!req.http.X-Timer) {
      set req.http.X-Timer = "S" time.start.sec "." time.start.usec_frac;
    }
    set req.http.X-Timer = req.http.X-Timer ",VS0";
  }

    
    set req.http.Fastly-Orig-Host = req.http.host;
    set req.http.host = "www.idg.tv";
            

    
  # default conditions
  set req.backend = F_idge1_usw_www_idgesg_idg_tv;
  

  
  # end default conditions

  # Request Condition: impossible_condition Prio: 10
  if( req.url ~ "^/jennyigotsyo#-in333dz2makeyoumIn-8675309$" ) {
        
    set req.backend = F_apollo_sitemaps_s3;
    
  
    
  }
  #end condition
  
          
  
#--FASTLY RECV END

    if (req.request != "HEAD" && req.request != "GET" && req.request != "FASTLYPURGE") {
      return(pass);
    }


## Call S3 For Sitemaps
  if( req.url ~ "^/(seo/sitemap.*|sitemap.xml$)"  ) {
    set req.url = regsuball(req.url, "(.*)", "/idgtv\1");
    set req.http.host = "apollo-sitemaps.s3-website-us-east-1.amazonaws.com";
    set req.backend = F_apollo_sitemaps_s3;
  }

# Custom Cookie Detection
## Included in vcl_recv

# limit purging via goodguys acl
  if (req.request ~ "PURGE" && ! (client.ip ~ goodguys)) {
    error 403 "Forbidden";
  }


      
        
## saved code
#  if ( req.http.Cookie && req.http.Cookie ~ "LtpaToken=") {
#if(req.url ~ "^/(\?|&).*") {
#    error 751 "http://site" + req.url;
#    error 751 "Zooty";
#    return(error);
 #   set req.url = regsuball(req.url, "(.*)", "\1?sean");
                #set req.http.Location = "http://www.bar.com" + req.url;
#  }

#if(req.url ~ "^/foo/.*") {
# if (req.http.host == "www.example.com") {


# Custom Argument Whitelist
## Included in vcl_recv


## sort arguments to increase cache hits
if(req.url ~ "(\?|&)") {
set req.url = boltsort.sort(req.url);
}


####################
## Blacklist all for select URLs
####################
# IDG.tv needs start= for the homepage
#if(req.url ~ "^/(\?|&).*") {
#set req.url = regsuball(req.url, ".*", "/");
#return(lookup);
#} #end if

####################
## Whitelist all for select URLs
####################

if(req.url ~ "(\?|&)") {
	if(req.url ~ "^/(godigital|ads\/bing|techpapers\/submit|napi)") {
		return(lookup);
	} #end if
} #end if

####################
## Whitelist all other urls
####################
## only evaluate if the url has an argument
if(req.url ~ "(\?|&)") {

# properly removes all arguments that are not in the whitelist
set req.url = regsuball(req.url, "([\?|&])(?!((?i)(action|aid|ajax|ajaxSearchType|APIClientID|APIVersion|author|bc|brandId|blxContentId|c|callback|caller|categoryId|categorySelect|catId|catSlug|cds_record_status|ch|chld|cid|collection|companyId|contentClass|contentType|ct|d|days|dc|dct|def|delay|displayId|displayTypeId|divId|dt|email|emailadd|EMAIL|emailAddr|endDate|f|featureClass|filters|fname|full|google_editors_picks|hackHTML|height|id|ids|iframe|init|isBroll|jobTitle|m|maxPrice|maxRows|minPrice|minWeight|mobileQuery|mode|mt|nl_name|NotLoggedIn|nsdr|page|pageNumber|pageSize|pageType|pagination|partnerCategory.catzeroId|partnerCategory.pageId|partnerCategory.topcatId|password|passwordConfirm|pid|platform|pretty|priceRange|prodid|productId|q|qt|query|region|resourceTypeId|reviewID|rid|rtypeSelect|s|searchType|sectionID|size|sort|sortBy|sortOrder|source|sourceid|sponsor|sponsorSelect|ss|start|startDate|startIndex|style|submit|submitForm|submitted|subsource|t|tag|threadId|timestamp|token|type|typeId|typeID|url|usercaptcha_response|username|userType|v|videoenv|width|yearsInIt|zpt)(=|&|$)))[^&]+", "\1");

####################
## Cleanup
####################
# leaves orphaned &s & ?s so we clean up below
# reduce multiple &s
set req.url = regsuball(req.url, "&+", "&");
# fix ?& to ?
set req.url = regsuball(req.url, "\?&", "\?");
# strip trailing & or ?
set req.url = regsuball(req.url, "[\?|&]+$", "");

# test added arg
#set req.url = regsuball(req.url, "(.*)", "\1?sean");

return(lookup);

} # end if


## clean version
##set req.url = regsuball(req.url, "([\?|&])(?!((foo|bar|yo)(=|&|$)))[^&]+", "\1");

## regex explained:
#
# (match arg start & or ?)
#([\?|&])
#
# do not match args that are foo follwed by =,& or end of the arg
# the (?i) makes them case insensitive
#(?!((?i)(foo|bar|yo)(=|&|$)))
#
# do match everything else that isn't an &
#[^&]+
#
# replace the match with the first captured ? or & 
#"\1"


# custom ttl


    return(lookup);
}


sub vcl_fetch {



#--FASTLY FETCH BEGIN


# record which cache ran vcl_fetch for this object and when
  set beresp.http.Fastly-Debug-Path = "(F " server.identity " " now.sec ") " if(beresp.http.Fastly-Debug-Path, beresp.http.Fastly-Debug-Path, "");

# generic mechanism to vary on something
  if (req.http.Fastly-Vary-String) {
    if (beresp.http.Vary) {
      set beresp.http.Vary = "Fastly-Vary-String, "  beresp.http.Vary;
    } else {
      set beresp.http.Vary = "Fastly-Vary-String, ";
    }
  }
  
    
  
 # priority: 0

 
      
  # Header rewrite delete origin cookie : 10
  
      
            unset beresp.http.Set-Cookie;
          
  
 
 
      
#--FASTLY FETCH END


# Remove all cookies from origin response
unset beresp.http.Set-Cookie;

 if ((beresp.status == 500 || beresp.status == 503) && req.restarts < 1 && (req.request == "GET" || req.request == "HEAD")) {
   restart;
 }

 if(req.restarts > 0 ) {
   set beresp.http.Fastly-Restarts = req.restarts;
 }

 if (beresp.http.Set-Cookie) {
   set req.http.Fastly-Cachetype = "SETCOOKIE";
   return (pass);
 }

 if (beresp.http.Cache-Control ~ "private") {
   set req.http.Fastly-Cachetype = "PRIVATE";
	set beresp.ttl = 0s;
   set beresp.grace = 0s;
   return (deliver);
   #return (pass);
 }

 if (beresp.status == 500 || beresp.status == 503) {
   set req.http.Fastly-Cachetype = "ERROR";
   set beresp.ttl = 1s;
   set beresp.grace = 5s;
   return (deliver);
 }

  if (beresp.http.Expires || beresp.http.Surrogate-Control ~ "max-age" || beresp.http.Cache-Control ~"(s-maxage|max-age)") {
    # keep the ttl here
  } else {
    # apply the default ttl
    set beresp.ttl = 3600s;
  }

## custom ttl
## included in vcl_fetch

#set Surrogate-Key header for cache clearing by AID
if(req.url ~ ".*video\?id=.*") {
set beresp.http.Surrogate-Key = regsuball(req.url, ".*id=([0-9]+).*", "\1" ) ;
set beresp.http.X-Surrogate-Key = beresp.http.Surrogate-Key ;
}


if (parse_time_delta(subfield(beresp.http.Edge-Control, "downstream-ttl")) >= 0) {
    set beresp.http.Cache-Control = "max-age=" parse_time_delta(subfield(beresp.http.Edge-Control, "downstream-ttl"));
	return (deliver);
}
if (parse_time_delta(subfield(beresp.http.Edge-Control, "cache-maxage")) >= 0) {
    set beresp.ttl = parse_time_delta(subfield(beresp.http.Edge-Control, "cache-maxage"));
	return (deliver);
}
if (parse_time_delta(subfield(beresp.http.Surrogate-Control, "max-age")) >= 0) {
    set beresp.ttl = parse_time_delta(subfield(beresp.http.Cache-Control, "max-age"));
    return (deliver);
}
if (parse_time_delta(subfield(beresp.http.Cache-Control, "s-maxage")) >= 0) {
    set beresp.ttl = parse_time_delta(subfield(beresp.http.Cache-Control, "max-age"));
    return (deliver);
}
if (parse_time_delta(subfield(beresp.http.Cache-Control, "max-age")) >= 0) {
    set beresp.ttl = parse_time_delta(subfield(beresp.http.Cache-Control, "max-age"));
    return (deliver);
}



##########################################################################
## Pass to origin, do not cache
#	if (req.url ~ "^\/newsletters\/nl_module_processor.*") { set beresp.ttl = 0s; set beresp.grace = 0s; return (deliver);}
#	if (req.url ~ "^\/godigital/") { set beresp.ttl = 0s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/ads\/bing.*") { set beresp.ttl = 0s; set beresp.grace = 0s; return (deliver);}
#	if (req.url ~ "^\/cds\/PrePopGatewayResponse.do") { set beresp.ttl = 0s; set beresp.grace = 0s; return (deliver);}
#	if (req.url ~ "^\/techpapers\/submit(/?$|/.+)") { set beresp.ttl = 0s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/user(/?$|/.+)") { set beresp.ttl = 0s; set beresp.grace = 0s; return (deliver);}
#	if (req.url ~ "^\/api\/registration\.json.*") { set beresp.ttl = 0s; set beresp.grace = 0s; return (deliver);}
#	if (req.url ~ "^\/articleComment\/get\.do.*?[?|&]username\="){ set beresp.ttl = 0s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/captcha\/.*") { set beresp.ttl = 0s; set beresp.grace = 0s; return (deliver);}


##########################################################################
# allow override for testing
 # priority: 10
 #if ( req.http.User-Agent == "Yo Gabba Gabba" ) {
#	set beresp.ttl = 5s;
#	set beresp.http.X-fastly-ttl = beresp.ttl ;
#	set beresp.grace = 1800s;
#	set beresp.http.X-fastly-stale = beresp.grace ;
#	return(deliver);
 # }
  
        
        
##########################################################################        
## Default if nothing set by origin

if ( beresp.http.Cache-Control !~ ".*max-age\=.*" ) {
	set beresp.ttl = 300s;
	set beresp.grace = 5d;
	# set headers
	set beresp.http.Cache-Control = "max-age=" regsuball(beresp.ttl, "\.[0-9]+", ""); # strip decimals from ttl
	set beresp.http.X-fastly-ttl = beresp.ttl ;
	set beresp.http.X-fastly-stale = beresp.grace ;
	return(deliver);
}

# else set the stale header
#	set beresp.grace = 2678400s;
#	set beresp.http.X-fastly-stale = beresp.grace ;




  return(deliver);
}

sub vcl_hit {
#--FASTLY HIT BEGIN

# we cannot reach obj.ttl and obj.grace in deliver, save them when we can in vcl_hit
  set req.http.Fastly-Tmp-Obj-TTL = obj.ttl;
  set req.http.Fastly-Tmp-Obj-Grace = obj.grace;

  {
    set req.http.Fastly-Cachetype = "HIT";

    
  }
#--FASTLY HIT END



  if (!obj.cacheable) {
    return(pass);
  }
  return(deliver);
}

sub vcl_miss {
#--FASTLY MISS BEGIN
  

# this is not a hit after all, clean up these set in vcl_hit
  unset req.http.Fastly-Tmp-Obj-TTL;
  unset req.http.Fastly-Tmp-Obj-Grace;

  {
    if (req.http.Fastly-Check-SHA1) {
       error 550 "Doesnt exist";
    }
    
#--FASTLY BEREQ BEGIN
    {
      if (req.http.Fastly-Original-Cookie) {
        set bereq.http.Cookie = req.http.Fastly-Original-Cookie;
      }
      
      if (req.http.Fastly-Original-URL) {
        set bereq.url = req.http.Fastly-Original-URL;
      }
      {
        if (req.http.Fastly-FF) {
          set bereq.http.Fastly-Client = "1";
        }
      }
      {
        # do not send this to the backend
        unset bereq.http.Fastly-Original-Cookie;
        unset bereq.http.Fastly-Original-URL;
        unset bereq.http.Fastly-Vary-String;
        unset bereq.http.X-Varnish-Client;
      }
      if (req.http.Fastly-Temp-XFF) {
         if (req.http.Fastly-Temp-XFF == "") {
           unset bereq.http.X-Forwarded-For;
         } else {
           set bereq.http.X-Forwarded-For = req.http.Fastly-Temp-XFF;
         }
         # unset bereq.http.Fastly-Temp-XFF;
      }
    }
#--FASTLY BEREQ END


 #;

    set req.http.Fastly-Cachetype = "MISS";

    
  }
#--FASTLY MISS END
  return(fetch);
}

sub vcl_deliver {




#--FASTLY DELIVER BEGIN

# record the journey of the object, expose it only if req.http.Fastly-Debug.
  if (req.http.Fastly-Debug || req.http.Fastly-FF) {
    set resp.http.Fastly-Debug-Path = "(D " server.identity " " now.sec ") "
       if(resp.http.Fastly-Debug-Path, resp.http.Fastly-Debug-Path, "");

    set resp.http.Fastly-Debug-TTL = if(obj.hits > 0, "(H ", "(M ")
       server.identity
       if(req.http.Fastly-Tmp-Obj-TTL && req.http.Fastly-Tmp-Obj-Grace, " " req.http.Fastly-Tmp-Obj-TTL " " req.http.Fastly-Tmp-Obj-Grace " ", " - - ")
       if(resp.http.Age, resp.http.Age, "-")
       ") "
       if(resp.http.Fastly-Debug-TTL, resp.http.Fastly-Debug-TTL, "");

    set resp.http.Fastly-Debug-Digest = digest.hash_sha256(req.digest);
  } else {
    unset resp.http.Fastly-Debug-Path;
    unset resp.http.Fastly-Debug-TTL;
  }

  # add or append X-Served-By/X-Cache(-Hits)
  {

    if(!resp.http.X-Served-By) {
      set resp.http.X-Served-By  = server.identity;
    } else {
      set resp.http.X-Served-By = resp.http.X-Served-By ", " server.identity;
    }

    set resp.http.X-Cache = if(resp.http.X-Cache, resp.http.X-Cache ", ","") if(fastly_info.state ~ "HIT($|-)", "HIT", "MISS");

    if(!resp.http.X-Cache-Hits) {
      set resp.http.X-Cache-Hits = obj.hits;
    } else {
      set resp.http.X-Cache-Hits = resp.http.X-Cache-Hits ", " obj.hits;
    }

  }

  if (req.http.X-Timer) {
    set resp.http.X-Timer = req.http.X-Timer ",VE" time.elapsed.msec;
  }

  # VARY FIXUP
  {
    # remove before sending to client
    set resp.http.Vary = regsub(resp.http.Vary, "Fastly-Vary-String, ", "");
    if (resp.http.Vary ~ "^\s*$") {
      unset resp.http.Vary;
    }
  }
  unset resp.http.X-Varnish;


  # Pop the surrogate headers into the request object so we can reference them later
  set req.http.Surrogate-Key = resp.http.Surrogate-Key;
  set req.http.Surrogate-Control = resp.http.Surrogate-Control;

  # If we are not forwarding or debugging unset the surrogate headers so they are not present in the response
  if (!req.http.Fastly-FF && !req.http.Fastly-Debug) {
    unset resp.http.Surrogate-Key;
    unset resp.http.Surrogate-Control;
  }

  if(resp.status == 550) {
    return(deliver);
  }
  

  #default response conditions
    
              
      

  # Response Condition: NOT Prio: 10
  if( req.http.User-Agent ~ "jkhefhwiehfwo8e9839u89" ) {
        
    # syslog syslog_use
    log {"syslog 7L3Bo1bdUQV1DWKe82yOqc syslog.use :: "} req.http.Fastly-Client-IP {" "} {""-""} {" "} {""-""} {" "} now {" "} req.request {" "} req.url {" "} resp.status;
    
  }
  
#--FASTLY DELIVER END

  return(deliver);
}

sub vcl_error {
#--FASTLY ERROR BEGIN

  if (obj.status == 801) {
     set obj.status = 301;
     set obj.response = "Moved Permanently";
     set obj.http.Location = "https://" req.http.host req.url;
     synthetic {""};
     return (deliver);
  }

              
      
  if (req.http.Fastly-Restart-On-Error) {
    if (obj.status == 503 && req.restarts == 0) {
      restart;
    }
  }

  {
    if (obj.status == 550) {
      return(deliver);
    }
  }
#--FASTLY ERROR END


# custom error
## included in vcl_error

# Look for cookie and send to auth if not present
 if (obj.status == 750) {
                set obj.http.location = req.http.Location;
                set obj.status = 302;
                return (deliver);
        }


## Test code
# if (obj.status == 751) {
#set obj.http.X-Debug = obj.response ;
#    set obj.http.Content-Type = "text/html; charset=utf-8";
#    
#    synthetic {"
#<?xml version="1.0" encoding="utf-8"?>
#<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
# "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
#<html>
#  <head>
#    <title>"} obj.status " " obj.response {"</title>
#  </head>
#  <body>
#    <h1>Error "} obj.status " " obj.response {"</h1>
#    <p>"} obj.response {"</p>
#    <h3>Guru Meditation:</h3>
#    <p>XID: "} req.xid {"</p>
#    <hr>
#    <p>Varnish cache server</p>
#  </body>
#</html>
#"};
#    return (deliver);
#
#  }

}

sub vcl_pass {
#--FASTLY PASS BEGIN
  

  {
    
#--FASTLY BEREQ BEGIN
    {
      if (req.http.Fastly-Original-Cookie) {
        set bereq.http.Cookie = req.http.Fastly-Original-Cookie;
      }
      
      if (req.http.Fastly-Original-URL) {
        set bereq.url = req.http.Fastly-Original-URL;
      }
      {
        if (req.http.Fastly-FF) {
          set bereq.http.Fastly-Client = "1";
        }
      }
      {
        # do not send this to the backend
        unset bereq.http.Fastly-Original-Cookie;
        unset bereq.http.Fastly-Original-URL;
        unset bereq.http.Fastly-Vary-String;
        unset bereq.http.X-Varnish-Client;
      }
      if (req.http.Fastly-Temp-XFF) {
         if (req.http.Fastly-Temp-XFF == "") {
           unset bereq.http.X-Forwarded-For;
         } else {
           set bereq.http.X-Forwarded-For = req.http.Fastly-Temp-XFF;
         }
         # unset bereq.http.Fastly-Temp-XFF;
      }
    }
#--FASTLY BEREQ END


 #;
    set req.http.Fastly-Cachetype = "PASS";
  }

#--FASTLY PASS END
}

sub vcl_log {
# Custom Log
## included in vcl_deliver

if(req.http.Host ~ "idg.tv") {
log {"syslog 7L3Bo1bdUQV1DWKe82yOqc syslog.use :: "} "GROUP-IDGTV " req.http.Fastly-Client-IP {" ["} now {"] "} req.request {" "} req.http.Host {" "} req.url {" "} resp.status {" ["}  req.http.referer {"] ["} req.http.User-Agent {"] "} resp.http.Content-Length {" "} resp.http.X-Cache {" "} resp.http.Age {" "}  geoip.region  {" "} resp.http.X-Served-By ;
}

}

sub vcl_pipe {
#--FASTLY PIPE BEGIN
  {
     
    
#--FASTLY BEREQ BEGIN
    {
      if (req.http.Fastly-Original-Cookie) {
        set bereq.http.Cookie = req.http.Fastly-Original-Cookie;
      }
      
      if (req.http.Fastly-Original-URL) {
        set bereq.url = req.http.Fastly-Original-URL;
      }
      {
        if (req.http.Fastly-FF) {
          set bereq.http.Fastly-Client = "1";
        }
      }
      {
        # do not send this to the backend
        unset bereq.http.Fastly-Original-Cookie;
        unset bereq.http.Fastly-Original-URL;
        unset bereq.http.Fastly-Vary-String;
        unset bereq.http.X-Varnish-Client;
      }
      if (req.http.Fastly-Temp-XFF) {
         if (req.http.Fastly-Temp-XFF == "") {
           unset bereq.http.X-Forwarded-For;
         } else {
           set bereq.http.X-Forwarded-For = req.http.Fastly-Temp-XFF;
         }
         # unset bereq.http.Fastly-Temp-XFF;
      }
    }
#--FASTLY BEREQ END


    #;
    set req.http.Fastly-Cachetype = "PIPE";
    set bereq.http.connection = "close";
  }
#--FASTLY PIPE END

}

sub vcl_hash {

  #--FASTLY HASH BEGIN

                
  
  #if unspecified fall back to normal
  {
    

    set req.hash += req.url;
    set req.hash += req.http.host;
    set req.hash += "#####GENERATION#####";
    return (hash);
  }
  #--FASTLY HASH END


}

